<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller extends CI_Controller {
	 
	public function index()
	{
		if ($this->session->userdata('username')) {
			$this->template->dashboard('vdashboard');
		}else {
			redirect('auth');
		}
	}

	public function view_reports()
	{
		if ($this->session->userdata('username')) {
			$this->template->dashboard('vreports');
		}else {
			redirect('auth');
		}
	}

	public function view_data_karyawan()
	{
		if ($this->session->userdata('username')) {
			$this->template->dashboard('vdata_karyawan');
		}else {
			redirect('auth');
		}
	} 

	public function view_settings()
	{
		if ($this->session->userdata('username')) {
			$this->template->dashboard('vsettings');
		}else {
			redirect('auth');
		}
	} 

	public function view_akun()
	{
		if ($this->session->userdata('username')) {
			$this->template->dashboard('vakun');
		}else {
			redirect('auth');
		}
	} 

	public function view_user_guide()
	{
		if ($this->session->userdata('username')) {
			$this->template->dashboard('vuser_guide');
		}else {
			redirect('auth');
		}
	} 


	public function cekUID()
	{     
		date_default_timezone_set("Asia/Jakarta");

		$UID = $this->input->get('ID');
		$cek = $this->Dbase->cekUID($UID)->num_rows(); 
		$absensi = $this->Dbase->absensiByIdCard($UID, date('Y-m-d'))->get(); 
		$schedule = $this->Dbase->get('tb_schedule')->row(); 
 
		// $time_now = '06:30:00';
		$time_now = date('H:i:s');
		$status = "";
		$keterangan = "";

		if ($cek > 0) { 
			if ($absensi->num_rows()){
				// jika sudah check in
				if ($time_now >= $schedule->s_out && $time_now <= $schedule->s_out_) {
					$status = "Check Out";

					$data = array(
						'time_out' => date('Y-m-d H:i:s'),
						'status' => $status,
						'keterangan_out' => "Tepat Waktu"
					);
	
					$absensi = $absensi->first_row();
 
					$update = $this->Dbase->update('tb_absensi', ['id_absensi' => $absensi->id_absensi], $data);
					if ($update) {
						echo "check_out_oke";
					}else {
						echo "gagal_check_out";
					}
				} else if($time_now > $schedule->s_out_) {
					$status = "Pulang";

					$data = array(
						'time_out' => date('Y-m-d H:i:s'),
						'status' => $status,
						'keterangan_out' => "Telat"
					);
	
					$absensi = $absensi->first_row();
		
					$update = $this->Dbase->update('tb_absensi', ['id_absensi' => $absensi->id_absensi], $data);

					echo "tidak_check_out";
				} else {
					//echo "Belum waktunya check out";
					echo "belum_check_out";
				}

			}else {
				// jika belum check in
				if ($time_now >= $schedule->s_in && $time_now <= $schedule->s_in_) {
					$status = "Check In";
					$keterangan = "Tepat Waktu";
					$this->storeAbsensi($status, $keterangan, $UID); 

					echo "check_in_oke";
				}elseif($time_now > $schedule->s_in_ && $time_now < $schedule->s_out) {
					$status = "Check In";
					$keterangan = "Telat";
					$this->storeAbsensi($status, $keterangan, $UID); 

					echo "check_in_oke";
				}else {
					//echo "Anda tidak dapat check in"; 
					echo "gagal_check_in";
				}
			}
		}else {   
			echo "gagal	";
		}
	}

	// function untuk input ke absensi
	private function storeAbsensi($status, $keterangan, $UID)
	{
		$data = array(
			'id_card' => $UID,
			'time_in' => date('Y-m-d H:i:s'), 
			'status' => $status,
			'keterangan_in' => $keterangan,
			'keterangan_out' => "Tanpa Keterangan",

		);
		
		if ($this->Dbase->storeToAbsensi($data)) {
			
		}
	}

	public function insert_new_user()
	{ 
		$UID = $this->input->get('ID');
		$IdMesin = $this->input->get('IdMesin');
		$cek = $this->Dbase->cekUID($UID)->num_rows();   
		  
		if ($IdMesin == 'MesinW001') {
			if ($cek > 0) {
				echo "ID sudah ada";
			}else {   
				$data = array(
					'id_card' 		=> $UID,
					'kode_jabatan'	=> 0
				);
				$this->Dbase->insert('tb_karyawan', $data);  
	
				echo "insert berhasil";
			} 
		} else {
			echo "Mesin Tidak Dikenal";
		}  
	}

	// public function insert_user_data()
	// {
	// 	$this->form_validation->set_rules('id_card', 'ID Card', 'required');
	// 	$this->form_validation->set_rules('nik', 'NIK', 'required');
	// 	$this->form_validation->set_rules('nama', 'Nama', 'required');
	// 	$this->form_validation->set_rules('email', 'Email', 'required');
	// 	$this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required');
	// 	$this->form_validation->set_rules('alamat', 'Alamat', 'required');
	// 	$this->form_validation->set_rules('hp', 'No. Hp', 'required');
	// 	$this->form_validation->set_rules('jabatan', 'Jabatan', 'required');

	// 	$id_card 	= $this->input->post('id_card');
	// 	$nik 		= $this->input->post('nik');
	// 	$nama 		= $this->input->post('nama');
	// 	$email 		= $this->input->post('email');
	// 	$jk		 	= $this->input->post('jk');
	// 	$alamat 	= $this->input->post('alamat');
	// 	$hp 		= $this->input->post('hp');
	// 	$jabatan 	= $this->input->post('jabatan');  

	// 	if (!$this->form_validation->run())
	// 	{
	// 		echo validation_errors();
	// 	}
	// 	else
	// 	{
	// 		$data = array(
	// 			'id_card'	=> $id_card,
	// 			'nik' 		=> $nik,
	// 			'nama' 		=> $nama,
	// 			'email' 	=> $email,
	// 			'jk' 		=> $jk,
	// 			'alamat' 	=> $alamat,
	// 			'hp' 		=> $hp,
	// 			'jabatan' 	=> $jabatan
	// 		); 
	// 		$this->Dbase->insert('tb_karyawan', $data);
	// 		redirect('controller/view_reports');
	// 	}
	// }

	public function update_data_karyawan()
	{
		$this->form_validation->set_rules('id_card', 'ID Card', 'required');
		$this->form_validation->set_rules('nik', 'NIK', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('hp', 'No. Hp', 'required');
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'required');

		$id_card 	= $this->input->post('id_card');
		$where 		= array('id_card' => $id_card);
		$nik 		= $this->input->post('nik');
		$nama 		= $this->input->post('nama');
		$email 		= $this->input->post('email');
		$jk		 	= $this->input->post('jk');
		$alamat 	= $this->input->post('alamat');
		$hp 		= $this->input->post('hp');
		$jabatan 	= $this->input->post('jabatan');   
		
		if (!$this->form_validation->run())
		{
			echo validation_errors();
		}
		else
		{
			$data = array( 
				'nik' 		=> $nik,
				'nama' 		=> $nama,
				'email' 	=> $email,
				'jk' 		=> $jk,
				'alamat' 	=> $alamat,
				'hp' 		=> $hp,
				'kode_jabatan' 	=> $jabatan
			); 

			$this->Dbase->update('tb_karyawan', $where, $data);
			redirect('data-karyawan');
		}
	}

	public function pencarian_data_karyawan($cari="")
	{
		header('Content-Type: application/json');

		$where = array('nama' => $cari);
		$query = $this->Dbase->cariKaryawan($where, 'nama');

		$data[] = array(); 
		foreach ($query->result() as $row) {   
			$data[] = array(  
				'id_card'	=> $row->id_card,
				'nik'		=> $row->nik,
				'nama'		=> $row->nama,
				'email'		=> $row->email, 
				'jk'		=> $row->jk, 
				'alamat'	=> $row->alamat,
				'hp'		=> $row->hp, 
				'jabatan'	=> $row->jabatan
			);    
		}     
		$output = array(
			'data' => $data
		);   
		
		echo json_encode($output); 
	}

	public function get_where_data_karyawan($id)
	{
		header('Content-Type: application/json');

		$where = array('id_card' => $id);
		$query = $this->Dbase->getWhereKaryawan($where, 'nama');

		$data[] = array(); 
		foreach ($query->result() as $row) {   
			$data[] = array(  
				'id_card'	=> $row->id_card,
				'nik'		=> $row->nik,
				'nama'		=> $row->nama,
				'email'		=> $row->email, 
				'jk'		=> $row->jk, 
				'alamat'	=> $row->alamat,
				'hp'		=> $row->hp, 
				'kode_jabatan'	=> $row->kode_jabatan,
				'jabatan'	=> $row->jabatan
			);    
		}     
		$output = array(
			'data' => $data
		);   
		
		echo json_encode($output); 
	}


	public function get_data_karyawan()
	{
		header('Content-Type: application/json');

		$query = $this->Dbase->getKaryawan('nama');

		$data[] = array(); 
		foreach ($query->result() as $row) {   
			$data[] = array(  
				'id_card'	=> $row->id_card,
				'nik'		=> $row->nik,
				'nama'		=> $row->nama,
				'email'		=> $row->email, 
				'jk'		=> $row->jk, 
				'alamat'	=> $row->alamat,
				'hp'		=> $row->hp, 
				'kode_jabatan'	=> $row->kode_jabatan,
				'jabatan'	=> $row->jabatan
			);    
		}     
		$output = array(
			'data' => $data
		);   
		
		echo json_encode($output); 
	}

	public function delete_karyawan($id)
	{ 
		$where = array('id_card' => $id);
		$query = $this->Dbase->delData('tb_karyawan', $where); 
	}

	public function get_data_schedule()
	{
		header('Content-Type: application/json');

		$query = $this->Dbase->get('tb_schedule');

		$data[] = array(); 
		foreach ($query->result() as $row) {   
			$data[] = array(  
				'kode_schedule'	=> $row->kode_schedule,
				's_in'			=> $row->s_in,
				's_in_'			=> $row->s_in_,
				's_out'			=> $row->s_out,
				's_out_'		=> $row->s_out_ 
			);    
		}     
		$output = array(
			'data' => $data
		);   
		
		echo json_encode($output); 
	}

	public function insert_data_schedule()
	{  
		$this->form_validation->set_rules('kode_schedule', 'Kode Schedule', 'required');
		$this->form_validation->set_rules('s_in', 'Schedule In', 'required');
		$this->form_validation->set_rules('s_in_', 'Schedule In', 'required');
		$this->form_validation->set_rules('s_out', 'Schedule Out', 'required'); 
		$this->form_validation->set_rules('s_out_', 'Schedule Out', 'required'); 

		$kode_schedule 	= $this->input->post('kode_schedule');
		$s_in 			= $this->input->post('s_in');
		$s_in_ 			= $this->input->post('s_in_');
		$s_out 			= $this->input->post('s_out');   
		$s_out_			= $this->input->post('s_out_');   

		if (!$this->form_validation->run())
		{
			echo validation_errors();
		}
		else
		{
			$data = array(
				'kode_schedule'	=> $kode_schedule,
				's_in' 			=> $s_in,
				's_in_'			=> $s_in_,
				's_out' 		=> $s_out,
				's_out_' 		=> $s_out_ 
			); 
			$this->Dbase->insert('tb_schedule', $data);
			redirect('settings');
		}
	}

	public function update_data_schedule()
	{
		$this->form_validation->set_rules('kode_schedule', 'Kode Schedule', 'required');
		$this->form_validation->set_rules('s_in', 'Schedule In', 'required');
		$this->form_validation->set_rules('s_in_', 'Schedule In', 'required');
		$this->form_validation->set_rules('s_out', 'Schedule Out', 'required'); 
		$this->form_validation->set_rules('s_out_', 'Schedule Out', 'required'); 

		$kode_schedule 	= $this->input->post('kode_schedule');
		$s_in 			= $this->input->post('s_in');
		$s_in_ 			= $this->input->post('s_in_');
		$s_out 			= $this->input->post('s_out');   
		$s_out_			= $this->input->post('s_out_');    
		
		if (!$this->form_validation->run())
		{
			echo validation_errors();
		}
		else
		{
			$data = array(
				'kode_schedule'	=> $kode_schedule,
				's_in' 			=> $s_in,
				's_in_'			=> $s_in_,
				's_out' 		=> $s_out,
				's_out_' 		=> $s_out_ 
			); 

			$this->Dbase->update('tb_schedule', array('kode_schedule' => $kode_schedule), $data);
			redirect('settings');
		}
	}

	public function get_where_data_schedule($id)
	{
		header('Content-Type: application/json');

		$where = array('kode_schedule' => $id);
		$query = $this->Dbase->getWhere('tb_schedule', $where, '');

		$data[] = array(); 
		foreach ($query->result() as $row) {   
			$data[] = array(  
				'kode_schedule'	=> $row->kode_schedule,
				's_in'			=> $row->s_in,
				's_in_'			=> $row->s_in_,
				's_out'			=> $row->s_out,
				's_out_'		=> $row->s_out_ 
			);    
		}     
		$output = array(
			'data' => $data
		);   
		
		echo json_encode($output); 
	} 

	public function delete_schedule($id)
	{ 
		$where = array('kode_schedule' => $id);
		$query = $this->Dbase->delData('tb_schedule', $where); 
	}

	public function get_data_jabatan()
	{
		header('Content-Type: application/json');

		$query = $this->Dbase->get('tb_jabatan', 'kode_jabatan');

		$data[] = array(); 
		foreach ($query->result() as $row) {   
			$data[] = array(  
				'kode_jabatan'	=> $row->kode_jabatan,
				'jabatan'		=> $row->jabatan 
			);    
		}     
		$output = array(
			'data' => $data
		);   
		
		echo json_encode($output); 
	}

	public function insert_data_jabatan()
	{  
		$this->form_validation->set_rules('kode_jabatan', 'Kode Jabatan', 'required');
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'required'); 

		$kode_jabatan 	= $this->input->post('kode_jabatan');
		$jabatan 		= $this->input->post('jabatan'); 
 
		if (!$this->form_validation->run())
		{
			echo validation_errors();
		}
		else
		{
			$data = array(
				'kode_jabatan'	=> $kode_jabatan,
				'jabatan' 		=> $jabatan 
			); 
			$this->Dbase->insert('tb_jabatan', $data);
			redirect('settings');
		}
	}

	public function update_data_jabatan()
	{
		$this->form_validation->set_rules('kode_jabatan', 'kode_jabatan', 'required');
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'required');

		$kode_jabatan 	= $this->input->post('kode_jabatan');
		$where = array('kode_jabatan' => $kode_jabatan);
		$jabatan 		= $this->input->post('jabatan');   
		
		if (!$this->form_validation->run())
		{
			echo validation_errors();
		}
		else
		{
			$data = array('jabatan' => $jabatan); 

			$this->Dbase->update('tb_jabatan', $where, $data);
			redirect('settings');
		}
	}

	public function get_where_data_jabatan($id)
	{
		header('Content-Type: application/json');

		$where = array('kode_jabatan' => $id);
		$query = $this->Dbase->getWhere('tb_jabatan', $where, '');

		$data[] = array(); 
		foreach ($query->result() as $row) {   
			$data[] = array(  
				'kode_jabatan'	=> $row->kode_jabatan,
				'jabatan'		=> $row->jabatan
			);    
		}     
		$output = array(
			'data' => $data
		);   
		
		echo json_encode($output); 
	} 

	public function delete_jabatan($id)
	{ 
		$where = array('kode_jabatan' => $id);
		$query = $this->Dbase->delData('tb_jabatan', $where); 
	}
}





