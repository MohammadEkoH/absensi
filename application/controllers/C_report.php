
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_report extends CI_Controller {

    public function pencarian_data_harian($cari="")
	{
		header('Content-Type: application/json');  
		$query = $this->Dbase->cariReport($cari)->result(); 
 
		echo json_encode($query); 
	}

	public function cetak()
	{
		$this->load->view('v_cetak');
	}

}
