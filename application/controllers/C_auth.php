
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_auth extends CI_Controller {
	function index()
	{
		if ($this->session->userdata('username')) {
			redirect('dashboard');
		}else {
			$this->load->view('vlogin'); 
		}
	}

	function cek_login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password'); 
		$password = md5($password);

		$sess = array('username' => $username);
		$this->session->set_userdata($sess);
		
		$where = array('username' => $username, 'password' =>$password);
		$query = $this->Dbase->auth($where); 

		if ($query) {
			redirect('dashboard');
		}else{
			$this->session->sess_destroy();
			redirect('auth'); 
		}
	} 

	public function reset_password()
	{
		$this->load->view('vreset_password');
	}

	public function logout()
	{ 
		$this->session->sess_destroy(); 
		redirect('auth');
	} 

	function edit_pass(){ 
		$username = $this->session->userdata('username');    

		$lama		= md5($this->input->post('password_lama'));
		$baru		= $this->input->post('password_baru');
		$confirm	= $this->input->post('password_confirm');

		$where = array(
			'username'	=> $username,
			'password' 	=> $lama
		);

		$cek = $this->Dbase->getWhere('tb_admin', $where)->row(); 

		$pass = $cek->password; 

		if($pass == $lama &&  $baru == $confirm){
			$where_id = array( 'username' => $username ); 
			$new = array('password' => md5($baru));
			
			$this->Dbase->update('tb_admin', $where_id, $new);

			redirect('dashboard');
		}else{
			redirect('akun');
		}	

	}

	function edit_email(){ 
		$username = $this->session->userdata('username');    
 
		$email		= $this->input->post('email');
		$password	= md5($this->input->post('password'));

		$where = array(
			'username'	=> $username,
			'password' 	=> $password
		);

		$cek = $this->Dbase->getWhere('tb_admin', $where)->row(); 

		$pass = $cek->password;  

		if($pass == $password){
			$where_id = array( 'username' => $username ); 
			$email = array('email' => $email); 
			
			$this->Dbase->update('tb_admin', $where_id, $email);

			redirect('dashboard');
		}else{
			redirect('akun');
		}	

	}
}
