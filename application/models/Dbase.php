<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dbase extends CI_Model {

    function auth($where)
	{  
		$this->db->limit(1);
		$query = $this->db->get_where('tb_admin', $where);
        if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
    }
    
    public function insert($table, $data)
    {
        $this->db->insert($table, $data);
    }

    public function update($table, $where, $data)
    {
        $this->db->where($where);
        return $this->db->update($table, $data);
    }

    public function getKaryawan($asc='')
	{
        $this->db->select('*')
                 ->from('tb_karyawan')
                 ->join('tb_jabatan', 'tb_jabatan.kode_jabatan = tb_karyawan.kode_jabatan', 'inner');
        $this->db->order_by($asc, "ASC");
        $query = $this->db->get();
        return $query;
    }

    public function get($table, $asc='')
	{ 
        $this->db->order_by($asc, "ASC");
        $query = $this->db->get($table);
        return $query;
    }

    public function getWhere($table, $where, $asc='')
    {
		$this->db->order_by($asc, "ASC");  
        $query = $this->db->get_where($table, $where);
        return $query;
    } 

    public function delData($table, $where)
    {
        $this->db->delete($table, $where);
    }

    public function getWhereKaryawan($where, $asc='')
	{
        $this->db->select('*')
                 ->from('tb_karyawan')
                 ->join('tb_jabatan', 'tb_jabatan.kode_jabatan = tb_karyawan.kode_jabatan', 'inner');
        $this->db->order_by($asc, "ASC");
        $query = $this->db->where($where);
        $query = $this->db->get();
        return $query;
    }

    public function cari($table, $where, $asc='')
    {
        $this->db->order_by($asc, "ASC");  
        $this->db->like($where);    
        $query = $this->db->get($table);
        return $query;
    }

    public function cariKaryawan($where, $asc='')
	{
        $this->db->select('*')
                 ->from('tb_karyawan')
                 ->join('tb_jabatan', 'tb_jabatan.kode_jabatan = tb_karyawan.kode_jabatan', 'inner');
        $this->db->order_by($asc, "ASC"); 
        $this->db->like($where);    
        $query = $this->db->get();
        return $query;
    }

    public function cekUID($UID)
	{
		return $this->db->select('*') 
                        ->where('id_card', $UID) 
                        ->get('tb_karyawan');
    } 


    public function getjabatan($asc='')
	{
        $this->db->select('*')
                 ->from('tb_jabatan')
                 ->join('tb_jabatan', 'inner');
        $this->db->order_by($asc, "ASC");
        $query = $this->db->get();
        return $query;
    } 

    public function cariReport($where)
    {
        $query = "SELECT tb_karyawan.nik, tb_karyawan.nama, IF(DATE(time_in) = '$where', id_absensi, '') AS id_absensi, 
                        IF(DATE(time_in) = '$where', time_in, '') AS time_in, 
                        IF(DATE(time_in) = '$where', time_out, '') AS time_out, 
                        IF(DATE(time_in) = '$where', `status`,'Belum Hadir' ) AS `status`, 
                        IF(DATE(time_in) = '$where', `keterangan_in`,'Tanpa Keterangan' ) AS `keterangan_in`, 
                        IF(DATE(time_in) = '$where', `keterangan_out`,'Tanpa Keterangan' ) AS `keterangan_out` 
                    FROM tb_absensi
                    RIGHT OUTER JOIN tb_karyawan ON tb_karyawan.id_card = tb_absensi.id_card  
                    UNION 
                    SELECT tb_karyawan.nik, tb_karyawan.nama, 
                        IF(DATE(time_in) = '$where', id_absensi,'') AS id_absensi, 
                        IF(DATE(time_in) = '$where', time_in, '') AS time_in, 
                        IF(DATE(time_in) = '$where', time_out, '') AS time_out, 
                        IF(DATE(time_in) = '$where', `status`,'Belum Hadir' ) AS `status`, 
                        IF(DATE(time_in) = '$where', `keterangan_in`,'Tanpa Keterangan' ) AS `keterangan_in`, 
                        IF(DATE(time_in) = '$where', `keterangan_out`,'Tanpa Keterangan' ) AS `keterangan_out` 
                    FROM tb_absensi
                    LEFT OUTER JOIN tb_karyawan ON tb_karyawan.id_card = tb_absensi.id_card
                    WHERE DATE(time_in) = '$where' OR id_absensi IS NULL   
                    ORDER BY nama ASC" ;

        // $query = "SELECT tb_karyawan.nik, tb_karyawan.nama, IF(DATE(time_in) = '$where', id_absensi, '') AS id_absensi, 
        //                 IF(DATE(time_in) = '$where', time_in, '') AS time_in, 
        //                 IF(DATE(time_in) = '$where', time_out, '') AS time_out, 
        //                 IF(DATE(time_in) = '$where', `status`,'Belum Hadir' ) AS `status`, 
        //                 IF(DATE(time_in) = '$where', `keterangan_in`,'Tanpa Keterangan' ) AS `keterangan_in`, 
        //                 IF(DATE(time_in) = '$where', `keterangan_out`,'Tanpa Keterangan' ) AS `keterangan_out` 
        //             FROM tb_absensi
        //             RIGHT JOIN tb_karyawan ON tb_karyawan.id_card = tb_absensi.id_card  
        //             WHERE DATE(time_in) = '$where' 
        //             OR id_absensi IS NULL
        //             ORDER BY nama ASC" ;
 
        return $this->db->query($query);

        // return $this->db->select('tb_karyawan.nik, tb_karyawan.nama, tb_absensi.*')
        //                 ->from('tb_absensi')
        //                 ->join('tb_karyawan', 'tb_absensi.id_card = tb_karyawan.id_card', 'right')  
        //                 ->where(['DATE(time_in)' => $where])->get();
 
    }

    public function absensiByIdCard($id_card, $date)
    {
        return $this->db->select('*')
                        ->from('tb_absensi')
                        ->join('tb_karyawan', 'tb_karyawan.id_card = tb_absensi.id_card', 'inner')
                        ->where(['tb_absensi.id_card' => $id_card, 'DATE(time_in)' => $date]);
    }

    public function updateCheckOutAbsensi($data, $id)
    {
        $this->db->get_where('tb_absensi', ['id_absensi' => $id]);
        return $this->db->update('tb_absensi', $data);
    }

    public function storeToAbsensi($data)
    {
        return $this->db->insert('tb_absensi', $data);
    }
	
}
