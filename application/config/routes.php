<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
$route['default_controller'] = 'C_auth';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['auth'] = 'C_auth';
$route['cek-auth'] = 'C_auth/cek_login';
$route['logout'] = 'C_auth/logout';
$route['forgot-password'] = 'C_auth/reset_password';
$route['update-password'] = 'C_auth/edit_pass';
$route['update-email'] = 'C_auth/edit_email';

$route['dashboard'] = 'controller';
$route['reports'] = 'controller/view_reports';
$route['data-karyawan'] = 'controller/view_data_karyawan';
$route['settings'] = 'controller/view_settings';
$route['akun'] = 'controller/view_akun';
$route['user-guide'] = 'controller/view_user_guide';
$route['send-uid'] = 'controller/showUID';
$route['cek-uid'] = 'controller/cekUID';
$route['insert-new-user'] = 'controller/insert_new_user'; 
$route['insert-data-karyawan'] = 'controller/insert_data_karyawan';
$route['insert-data-schedule'] = 'controller/insert_data_schedule';
$route['insert-data-jabatan'] = 'controller/insert_data_jabatan';
$route['update-data-karyawan'] = 'controller/update_data_karyawan';
$route['update-data-jabatan'] = 'controller/update_data_jabatan';
$route['update-data-schedule'] = 'controller/update_data_schedule';
