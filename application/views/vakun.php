<div class="row">
    <div class="span6"> 
        <div class="widget widget-table action-table"> 
            <div class="widget-header"> <i class="icon-user"></i>  
                <h3 class="text-center"> Ubah Email </h3>  
            </div>   
            <div class="widget-content"> 
                <div class="span7"> 
                    <br>
                    <form action="update-email" method="post">
                        <input type="email" placeholder="Email" name="email" required> <br> <br>
                        <input type="password" placeholder="Password" name="password" required> <br>  
                        <button type="submit">Submit</button> <br><br>
                    </form>
                </div>
            </div> 
        </div> 
    </div>   
    <div class="span6">
        <div class="widget widget-table action-table"> 
            <div class="widget-header"> <i class="icon-user"></i>  
                <h3 class="text-center"> Ubah Password </h3>  
            </div>   
            <div class="widget-content"> 
                <div class="span7"> 
                    <br>
                    <form action="update-password" method="post">
                        <input type="password" placeholder="Password Lama" name="password_lama" required> <br> <br>
                        <input type="password" placeholder="Password Baru" name="password_baru" required> <br> 
                        <input type="password" placeholder="Confirm Password Baru"  name="password_confirm" required> <br>
                        <button type="submit">Submit</button> <br><br>
                    </form>
                </div>
            </div> 
        </div> 
    </div>  
</div>