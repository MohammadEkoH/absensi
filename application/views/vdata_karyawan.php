<div class="widget widget-table action-table"> 
    <div class="widget-header"> <i class="icon-user"></i>  
        <h3 class="text-center"> User Data </h3>  
        <div class="pull-right"> 
            <input type="text" class="search-query" id="cari-data-karyawan" placeholder="Search"> 
        </div> 
    </div>  
    <div class="widget-content">
        <div class="table-wrapper-scroll-x"> 
            <table class="table table-striped table-bordered" >
                <thead>
                    <tr>
                    <th> No. </th>  
                    <th> ID Card </th>
                    <th> NIK </th>
                    <th> Nama </th>
                    <th> Email </th>
                    <th> JK </th>
                    <th> Alamat </th>
                    <th> No. HP </th>
                    <th> Jabatan </th>
                    <th calss="td-actions"> Action </th>
                    </tr>
                </thead>
                <tbody id="data-karyawan">
                    
                </tbody>
            </table>
        </div>
        <div class="load-more" id="last_id" style="display: none;">
        
        </div>
    </div> 
</div>