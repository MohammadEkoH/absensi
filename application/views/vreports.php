
<div class="widget widget-table action-table">  
    <div class="widget-header"> <i class="icon-list-alt"></i>  
        <h3 class="text-center"> Data Harian </h3>  
        <div class="pull-right"> 
            <a href="<?=base_url()?>C_report/cetak" class="btn" id="btn-print" target="_BLANK" style="display:none"> Print <i class="icon-print"></i> </a>
            <input type="date" id="cari-date">
        </div> 
    </div>  
    <div class="widget-content">
        <div class="table-wrapper-scroll-x"> 
            <table class="table table-striped table-bordered">
                <thead> 
                    <tr>
                        <th width="20">No.</th>
                        <th width="120">NIK</th>
                        <th width="200">Nama</th>
                        <th>IN</th>
                        <th width="100">Keterangan IN</th>
                        <th>OUT</th>
                        <th width="100">Keterangan OUT</th>
                        <th width="100">Status</th>
                    </tr>
                    <tr>  
                    </tr>
                </thead>
                <tbody id="data-harian">
                    
                </tbody>
            </table>
        </div>
    </div> 
</div> 
<!-- 
<div class="widget widget-table action-table">  
    <div class="widget-header"> <i class="icon-list-alt"></i>  
        <h3 class="text-center"> Data Tahunan </h3>  
        <div class="pull-right"> 
            <input type="text" class="search-query" placeholder="Search"> 
        </div> 
    </div>  
    <div class="widget-content">
        <div class="table-wrapper-scroll-x"> 
            <table class="table table-striped table-bordered">
                <thead> 
                    <tr>
                        <th rowspan="2" style="text-align:center;" width="20"> No</th> 
                        <th rowspan="2" style="text-align:center;"> NIK</th> 
                        <th rowspan="2" style="text-align:center;"> Nama</th> 
                        <th colspan="12" style="text-align:center;"> Bulan</th> 
                    </tr>
                    <tr> 
                        <th> Jan</th>
                        <th> Feb</th>
                        <th> Mar</th>
                        <th> Apr</th>
                        <th> Mei</th>
                        <th> Jun</th>
                        <th> Jul</th>
                        <th> Agt</th>
                        <th> Sep</th>
                        <th> Okt</th>
                        <th> Nob</th>
                        <th> Des</th>
                    </tr>
                </thead>
                <tbody id="data-user">
                    <tr>
                        <td>1</td>
                        <td>1234567890</td>
                        <td>Mohammad Eko Hardiyanto</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div> 
</div> -->