
<!-- Modal del karyawan -->
<div id="modal-del-karyawan" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header"> 
        <h3 id="myModalLabel">Apakah anda yakin?</h3>
    </div> 
    <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <button class="btn btn-danger btn-del-karyawan">Delete</button>
    </div>
</div> 

<!-- Modal edit karyawan -->
<div id="modal-edit-karyawan" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Edit Data</h3>
    </div>
    <div class="modal-body">
        <form action="<?=base_url()?>update-data-karyawan" method="post">
            <div class="col-md-12" align="center"> 
                <input disabled id="uid" required>
                <input type="hidden" id="uuid" name="id_card" required>
                <input type="text" placeholder="NIK" id="nik" name="nik" required>
                <input type="text" placeholder="Nama" id="nama" name="nama" required>
                <input type="email" placeholder="Email" id="email" name="email" required>
                <select id="jk" name="jk" required> 
                    <option value="L">L</option>
                    <option value="P">P</option>
                </select>
                <input type="text" placeholder="Alamat" id="alamat" name="alamat" required>
                <input type="text" placeholder="No. Hp" id="hp" name="hp" required>
                <select name="jabatan" id="get_jabatan">
                    
                </select>
            </div>
        </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                <button class="btn btn-primary" type="submit">Update</button>
        </form>
    </div>
</div>

<!-- Modal Insert Schedule -->
<div id="modal-insert-shedule" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Insert Schedule</h3>
    </div>
    <div class="modal-body">
        <form action="<?=base_url()?>insert-data-schedule" method="post">
            <div class="col-md-12" align="center"> 
                <label for="">Kode Schedule</label>
                <input type="text" placeholder="Kode Schedule" name="kode_schedule" required> <br>
                <label for="">Schedule In</label>
                <input type="time" placeholder="Schedule in"name="s_in" required> - 
                <input type="time" placeholder="Schedule in" name="s_in_" required> <br>
                <label for="">Schedule Out</label>
                <input type="time" placeholder="Schedule out" name="s_out" required> -
                <input type="time" placeholder="Schedule out" name="s_out_" required>  
            </div>
        </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                <button class="btn btn-primary" type="submit">Submit</button>
        </form>
    </div>
</div>

<!-- Modal edit Schedule -->
<div id="modal-edit-schedule" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Edit Schedule</h3>
    </div>
    <div class="modal-body">
        <form action="<?=base_url()?>update-data-schedule" method="post">
            <div class="col-md-12" align="center"> 
                <input type="hidden" id="kode_schedule" name="kode_schedule" required> <br> 
                <label for="">Schedule In</label>
                <input type="time" placeholder="Schedule in" id="s_in" name="s_in" required> - 
                <input type="time" placeholder="Schedule in" id="s_in_" name="s_in_" required> <br>
                <label for="">Schedule Out</label>
                <input type="time" placeholder="Schedule out" id="s_out" name="s_out" required> -
                <input type="time" placeholder="Schedule out" id="s_out_" name="s_out_" required> 
            </div>
        </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                <button class="btn btn-primary" type="submit">Update</button>
        </form>
    </div>
</div>

<!-- Modal del schedule -->
<div id="modal-del-schedule" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header"> 
        <h3 id="myModalLabel">Apakah anda yakin?</h3>
    </div> 
    <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <button class="btn btn-danger btn-del-schedule">Delete</button>
    </div>
</div> 

<!-- Modal Insert Jabatan -->
<div id="modal-insert-jabatan" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Insert Jabatan</h3>
    </div>
    <div class="modal-body">
        <form action="<?=base_url()?>insert-data-jabatan" method="post">
            <div class="col-md-12" align="center"> 
                <label for="">Kode Jabatan</label>
                <input type="text" placeholder="Kode Jabatan" name="kode_jabatan" required> <br>
                <label for="">Jabatan</label>
                <input type="text" placeholder="Jabatan" name="jabatan" required> <br> 
            </div>
        </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                <button class="btn btn-primary" type="submit">Submit</button>
        </form>
    </div>
</div> 

<!-- Modal edit jabatan -->
<div id="modal-edit-jabatan" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Edit Jabatan</h3>
    </div>
    <div class="modal-body">
        <form action="<?=base_url()?>update-data-jabatan" method="post">
            <div class="col-md-12" align="center"> 
                <input type="hidden" id="kode_jabatan" name="kode_jabatan" required> <br>
                <label for="">Kode Jabatan</label>
                <input disabled placeholder="Kode Jabatan" id="kode_jabatan_"> <br>
                <label for="">Jabatan</label>
                <input type="text" placeholder="Jabatan" id="jabatan" name="jabatan" required> <br> 
            </div>
        </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                <button class="btn btn-primary" type="submit">Update</button>
        </form>
    </div>
</div>

<!-- Modal del jabatan -->
<div id="modal-del-jabatan" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header"> 
        <h3 id="myModalLabel">Apakah anda yakin?</h3>
    </div> 
    <div class="modal-footer">
    <input type="hidden" id="get-kode-jabatan">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <button class="btn btn-danger btn-del-jabatan">Delete</button>
    </div>
</div> 