<div class="row">
    <div class="span6">
        <div class="widget widget-table action-table"> 
            <div class="widget-header"> <i class="icon-time"></i>  
                <h3 class="text-center"> Time Schedule </h3>  
                <div class="pull-right">   
                    <!-- <a href="" data-toggle="modal" data-target="#modal-insert-shedule" class="icon-plus" style="text-decoration:none"> &nbsp;&nbsp;&nbsp;</a> -->
                </div> 
            </div>   
            <div class="widget-content">
                <div class="table-wrapper-scroll-x"> 
                    <table class="table table-striped table-bordered" >
                        <thead>
                            <tr> 
                            <th> In </th> 
                            <th> Out </th> 
                            <th calss="td-actions" width="11%"> Action </th>
                            </tr>
                        </thead>
                        <tbody id="data-schedule">
                            
                        </tbody>
                    </table>
                </div>
            </div> 
        </div> 
    </div> 
    <!-- /span6 -->
    <div class="span6">
        <div class="widget widget-table action-table"> 
            <div class="widget-header"> <i class="icon-briefcase"></i>  
                <h3 class="text-center"> Jabatan </h3>  
                <div class="pull-right">   
                    <a href="" data-toggle="modal" data-target="#modal-insert-jabatan" class="icon-plus" style="text-decoration:none"> &nbsp;&nbsp;&nbsp;</a>
                </div> 
            </div>   
            <div class="widget-content">
                <div class="table-wrapper-scroll-x"> 
                    <table class="table table-striped table-bordered" >
                        <thead>
                            <tr>
                            <th> No. </th>  
                            <th> Kode </th>
                            <th> Jabatan </th> 
                            <th calss="td-actions"  width="20%"> Action </th>
                            </tr>
                        </thead>
                        <tbody id="data-jabatan">
                            
                        </tbody>
                    </table>
                </div>
            </div> 
        </div> 
    </div> 
</div>