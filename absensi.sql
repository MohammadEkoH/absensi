-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 11 Mar 2019 pada 17.34
-- Versi server: 10.1.31-MariaDB
-- Versi PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `absensi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_absensi`
--

CREATE TABLE `tb_absensi` (
  `id_absensi` int(11) NOT NULL,
  `id_card` varchar(15) DEFAULT NULL,
  `time_in` datetime DEFAULT NULL,
  `time_out` datetime DEFAULT NULL,
  `status` enum('Hadir','Pulang') DEFAULT NULL,
  `keterangan` enum('Tepat Waktu','Telat') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_admin`
--

INSERT INTO `tb_admin` (`id`, `username`, `email`, `password`) VALUES
(0, 'admin', 'admin@admin.com', '827ccb0eea8a706c4c34a16891f84e7b');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_detail_absensi`
--

CREATE TABLE `tb_detail_absensi` (
  `id_card` int(11) NOT NULL,
  `id_absensi` varchar(15) NOT NULL,
  `time_in` time NOT NULL,
  `time_out` time NOT NULL,
  `keterangan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jabatan`
--

CREATE TABLE `tb_jabatan` (
  `kode_jabatan` varchar(5) NOT NULL,
  `jabatan` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_jabatan`
--

INSERT INTO `tb_jabatan` (`kode_jabatan`, `jabatan`) VALUES
('0', '-Pilih Jabatan-'),
('001', 'GURU'),
('002', 'STAF'),
('003', 'KEPALA SEKOLAH');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_karyawan`
--

CREATE TABLE `tb_karyawan` (
  `id_card` varchar(15) NOT NULL,
  `nik` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `jk` char(1) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `hp` varchar(13) NOT NULL,
  `kode_jabatan` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_karyawan`
--

INSERT INTO `tb_karyawan` (`id_card`, `nik`, `nama`, `email`, `jk`, `alamat`, `hp`, `kode_jabatan`) VALUES
('0890273', '1231432423', '', '', '', '', '', '001'),
('0923213', '1234567890567', 'Zachfiandhika G. M.', 'zachfiandhikagm@gmail.com', 'L', 'nginden 2 no 104', '081231445665', '002'),
('1234566', '1234567890987', 'Pradipta Bagus Suryanda', 'pradipta01@gmail.com', 'L', 'Pandaan', '085205223881', '001'),
('1234567', '1234567890987', 'Mohammad Eko Hardiyanto', 'moh.eko.hardiyanto1998@gmail.com', 'L', 'Banyu Urip Lor 5/72A', '082233418595', '003'),
('3CA23D9', '1461600067', 'Jeffry Suyanto', 'jeff@gmail.com', 'L', 'Wonorejo', '088743284340', '001'),
('78G8767', '1234567890456', 'Ichwan Dwi Prastyo', 'ichwandpras@gmail.com', 'L', 'nginden 2 no. 10', '081331418758', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_mesin`
--

CREATE TABLE `tb_mesin` (
  `kode_mesin` int(11) NOT NULL,
  `nama_mesin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_schedule`
--

CREATE TABLE `tb_schedule` (
  `kode_schedule` varchar(10) NOT NULL,
  `s_in` time NOT NULL,
  `s_out` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_schedule`
--

INSERT INTO `tb_schedule` (`kode_schedule`, `s_in`, `s_out`) VALUES
('MALAM', '19:00:00', '01:00:00'),
('PAGI', '07:00:00', '00:30:00'),
('SIANG', '12:30:00', '16:00:00');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_absensi`
--
ALTER TABLE `tb_absensi`
  ADD PRIMARY KEY (`id_absensi`);

--
-- Indeks untuk tabel `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_jabatan`
--
ALTER TABLE `tb_jabatan`
  ADD PRIMARY KEY (`kode_jabatan`);

--
-- Indeks untuk tabel `tb_karyawan`
--
ALTER TABLE `tb_karyawan`
  ADD PRIMARY KEY (`id_card`,`nik`),
  ADD KEY `kode_jabatan` (`kode_jabatan`);

--
-- Indeks untuk tabel `tb_mesin`
--
ALTER TABLE `tb_mesin`
  ADD PRIMARY KEY (`kode_mesin`);

--
-- Indeks untuk tabel `tb_schedule`
--
ALTER TABLE `tb_schedule`
  ADD PRIMARY KEY (`kode_schedule`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_absensi`
--
ALTER TABLE `tb_absensi`
  MODIFY `id_absensi` int(11) NOT NULL AUTO_INCREMENT;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tb_karyawan`
--
ALTER TABLE `tb_karyawan`
  ADD CONSTRAINT `tb_karyawan_ibfk_1` FOREIGN KEY (`kode_jabatan`) REFERENCES `tb_jabatan` (`kode_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
