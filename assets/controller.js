
getDataKaryawan();
function getDataKaryawan()
{
  $(document).ready(function(){ 
    $.ajax({
    url: base_url + 'controller/get_data_karyawan',
    type: 'POST',
    dataType: "json",
    beforeSend: function(e) {
        
    }, 
    success: function(data){  
        let html = "";
        let no = 1;
        for (let i = 1; i < data.data.length; i++)
        {   
            html +=  '<tr>'+
                        '<td>'+no+'</td>'+
                        '<td>'+data.data[i].id_card+'</td>'+ 
                        '<td>'+data.data[i].nik+'</td>'+
                        '<td>'+data.data[i].nama+'</td>'+
                        '<td>'+data.data[i].email+'</td>'+
                        '<td>'+data.data[i].jk+'</td>'+
                        '<td>'+data.data[i].alamat+'</td>'+
                        '<td>'+data.data[i].hp+'</td>'+
                        '<td>'+data.data[i].jabatan+'</td>'+
                        '<td align="center">'+
                            '<button onclick="getWhereDataKaryawan(' + "'" + data.data[i].id_card + "'" +')" name="button" data-toggle="modal" data-target="#modal-edit-karyawan" title="edit"  class="btn btn-warning btn-xs icon-pencil"><i class="fa fa-pencil"></i></button> &nbsp;'+
                            '<button onclick="delKaryawan(' + "'" + data.data[i].id_card + "'" +')" name="button" data-toggle="modal" data-target="#modal-del-karyawan" title="delete"  class="btn btn-danger btn-xs icon-remove"><i class="fa fa-pencil"></i></button> &nbsp;'+
                        '</td>'+
                    '</tr>'; 
                    no++;
        }
        $('#data-karyawan').html(html);
    },
    error: function (xhr, ajaxOptions, thrownError) {
        console.error(xhr.responseText);
    }
    }); 
  });
}
 
function getWhereDataKaryawan(id)
{
  $(document).ready(function(){ 
    $.ajax({
    url: base_url + 'controller/get_where_data_karyawan/'+id,
    type: 'POST',
    dataType: "json",
    beforeSend: function(e) {
        
    }, 
    success: function(data){   
        for (let i = 1; i < data.data.length; i++)
        { 
            $('#last_id').val(data.data[i].id_card);
            $('#uid').val(data.data[i].id_card);
            $('#uuid').val(data.data[i].id_card);
            $('#nik').val(data.data[i].nik);
            $('#nama').val(data.data[i].nama);
            $('#email').val(data.data[i].email);
            $('#jk').val(data.data[i].jk);
            $('#alamat').val(data.data[i].alamat);
            $('#hp').val(data.data[i].hp);
            $('#get_jabatan').val(data.data[i].kode_jabatan); 
            
        } 
    },
    error: function (xhr, ajaxOptions, thrownError) {
        console.error(xhr.responseText);
    }
    }); 
  });
}

pencarianDataKaryawan();
function pencarianDataKaryawan()
{ 
  $(document).ready(function(){
    $("#cari-data-karyawan").keyup(function(){    
      $.ajax({
        url: base_url + 'controller/pencarian_data_karyawan/'+this.value,
        type: 'POST',
        dataType: "json",
        beforeSend: function(e) {
            
        }, 
        success: function(data){ 
          console.log(data);
          let html = "";
          let no = 1;
          for (let i = 1; i < data.data.length; i++)
          { 
            html +=  '<tr>'+
                        '<td>'+no+'</td>'+
                        '<td>'+data.data[i].id_card+'</td>'+ 
                        '<td>'+data.data[i].nik+'</td>'+
                        '<td>'+data.data[i].nama+'</td>'+
                        '<td>'+data.data[i].email+'</td>'+
                        '<td>'+data.data[i].jk+'</td>'+
                        '<td>'+data.data[i].alamat+'</td>'+
                        '<td>'+data.data[i].hp+'</td>'+
                        '<td>'+data.data[i].jabatan+'</td>'+
                        '<td align="center">'+
                        '<button onclick="getWhereDataKaryawan(' + "'" + data.data[i].id_card + "'" +')" name="button" data-toggle="modal" data-target="#modal-edit-karyawan" title="edit"  class="btn btn-warning btn-xs icon-pencil"><i class="fa fa-pencil"></i></button> &nbsp;'+
                        '<button onclick="delKaryawan(' + "'" + data.data[i].id_card + "'" +')" name="button" data-toggle="modal" data-target="#modal-del-karyawan" title="delete"  class="btn btn-danger btn-xs icon-remove"><i class="fa fa-pencil"></i></button> &nbsp;'+
                        '</td>'+
                    '</tr>'; 
                    no++;
          }
          $('#data-karyawan').html(html);
        },
        error: function (xhr, ajaxOptions, thrownError) {
          console.error(xhr.responseText);
        }
      });
    });
  });
}

function delKaryawan(id)
{  
  $(".btn-del-karyawan").click(function(){
    $(document).ready(function(){ 
      $.ajax({
      url: base_url + 'controller/delete_karyawan/'+id,
      type: 'POST',
      dataType: "json",
      beforeSend: function(e) {
          
      }, 
      success: function(data){
        $('#modal-del-karyawan').modal('hide');    
        getDataKaryawan();
      },
      error: function (xhr, ajaxOptions, thrownError) {
          console.error(xhr.responseText);
      }
      }); 
    });
  });
}

selectDataSchedule();
function selectDataSchedule()
{
  $(document).ready(function(){ 
    $.ajax({
    url: base_url + 'controller/get_data_schedule',
    type: 'POST',
    dataType: "json",
    beforeSend: function(e) {
        
    }, 
    success: function(data){  
      let option = "";  
      for (let i = 1; i < data.data.length; i++)
      {
        option += '<option value="'+data.data[i].kode_schedule+'">'+data.data[i].s_out+''+data.data[i].s_out+'</option>';
      }
      $('#schedule').html(option);
    },
    error: function (xhr, ajaxOptions, thrownError) {
        console.error(xhr.responseText);
    }
    }); 
  });
}

getDataSchedule();
function getDataSchedule()
{
  $(document).ready(function(){ 
    $.ajax({
    url: base_url + 'controller/get_data_schedule',
    type: 'POST',
    dataType: "json",
    beforeSend: function(e) {
        
    }, 
    success: function(data){  
        let html = ""; 
        for (let i = 1; i < data.data.length; i++)
        { 
            html +=  '<tr>'+ 
                        '<td>'+data.data[i].s_in+" - "+data.data[i].s_in_+'</td>'+ 
                        '<td>'+data.data[i].s_out+" - "+data.data[i].s_out_+'</td>'+ 
                        '<td align="center">'+
                            '<button onclick="getWhereDataSchedule(' + "'" + data.data[i].kode_schedule + "'" +')" name="button" data-toggle="modal" data-target="#modal-edit-schedule" title="edit"  class="btn btn-warning btn-xs icon-pencil"><i class="fa fa-pencil"></i></button> &nbsp;'+
                            // '<button onclick="delSchedule(' + "'" + data.data[i].kode_schedule + "'" +')" name="button" data-toggle="modal" data-target="#modal-del-schedule" title="delete"  class="btn btn-danger btn-xs icon-remove"><i class="fa fa-pencil"></i></button> &nbsp;'+ 
                        '</td>'+
                    '</tr>';  
        }
        $('#data-schedule').html(html);
    },
    error: function (xhr, ajaxOptions, thrownError) {
        console.error(xhr.responseText);
    }
    }); 
  });
}

selectDataJabatan();
function selectDataJabatan()
{
  $(document).ready(function(){ 
    $.ajax({
    url: base_url + 'controller/get_data_jabatan',
    type: 'POST',
    dataType: "json",
    beforeSend: function(e) {
        
    }, 
    success: function(data){  
      let option = "";  
      for (let i = 1; i < data.data.length; i++)
      {
        option += '<option value="'+data.data[i].kode_jabatan+'">'+data.data[i].jabatan+'</option>';
      }
      $('#get_jabatan').html(option);
    },
    error: function (xhr, ajaxOptions, thrownError) {
        console.error(xhr.responseText);
    }
    }); 
  });
}

function getWhereDataSchedule(id)
{
  $(document).ready(function(){ 
    $.ajax({
    url: base_url + 'controller/get_where_data_schedule/'+id,
    type: 'POST',
    dataType: "json",
    beforeSend: function(e) {
        
    }, 
    success: function(data){   
        for (let i = 1; i < data.data.length; i++)
        { 
            $('#kode_schedule').val(data.data[i].kode_schedule);
            $('#s_in').val(data.data[i].s_in);  
            $('#s_in_').val(data.data[i].s_in_);  
            $('#s_out').val(data.data[i].s_out);  
            $('#s_out_').val(data.data[i].s_out_);  
        } 
    },
    error: function (xhr, ajaxOptions, thrownError) {
        console.error(xhr.responseText);
    }
    }); 
  });
} 

function delSchedule(id)
{  
  $(".btn-del-schedule").click(function(){
    $(document).ready(function(){ 
      $.ajax({
      url: base_url + 'controller/delete_schedule/'+id,
      type: 'POST',
      dataType: "json",
      beforeSend: function(e) {
          
      }, 
      success: function(data){
        $('#modal-del-schedule').modal('hide');    
        getDataSchedule();
      },
      error: function (xhr, ajaxOptions, thrownError) {
          console.error(xhr.responseText);
      }
      }); 
    });
  });
}

getDatajabatan();
function getDatajabatan()
{
  $(document).ready(function(){ 
    $.ajax({
    url: base_url + 'controller/get_data_jabatan',
    type: 'POST',
    dataType: "json",
    beforeSend: function(e) {
        
    }, 
    success: function(data){  
        let html = "";
        let no = 1;
        for (let i = 2; i < data.data.length; i++)
        { 
            html +=  '<tr>'+
                        '<td>'+no+'</td>'+
                        '<td>'+data.data[i].kode_jabatan+'</td>'+ 
                        '<td>'+data.data[i].jabatan+'</td>'+  
                        '<td align="center">'+
                            '<button onclick="getWhereDataJabatan(' + "'" + data.data[i].kode_jabatan + "'" +')" name="button" data-toggle="modal" data-target="#modal-edit-jabatan" title="edit"  class="btn btn-warning btn-xs icon-pencil"><i class="fa fa-pencil"></i></button> &nbsp;'+
                            '<button onclick="delJabatan(' + "'" + data.data[i].kode_jabatan + "'" +')" name="button" data-toggle="modal" data-target="#modal-del-jabatan" title="delete"  class="btn btn-danger btn-xs icon-remove"><i class="fa fa-pencil"></i></button> &nbsp;'+ 
                        '</td>'+
                    '</tr>'; 
                    no++;
        }
        $('#data-jabatan').html(html);
    },
    error: function (xhr, ajaxOptions, thrownError) {
        console.error(xhr.responseText);
    }
    }); 
  });
}

function getWhereDataJabatan(id)
{
  $(document).ready(function(){ 
    $.ajax({
    url: base_url + 'controller/get_where_data_jabatan/'+id,
    type: 'POST',
    dataType: "json",
    beforeSend: function(e) {
        
    }, 
    success: function(data){   
        for (let i = 1; i < data.data.length; i++)
        { 
            $('#kode_jabatan').val(data.data[i].kode_jabatan);
            $('#kode_jabatan_').val(data.data[i].kode_jabatan);
            $('#jabatan').val(data.data[i].jabatan);  
        } 
    },
    error: function (xhr, ajaxOptions, thrownError) {
        console.error(xhr.responseText);
    }
    }); 
  });
} 

function delJabatan(id)
{  
  $(".btn-del-jabatan").click(function(){
    $(document).ready(function(){ 
      $.ajax({
      url: base_url + 'controller/delete_jabatan/'+id,
      type: 'POST',
      dataType: "json",
      beforeSend: function(e) {
          
      }, 
      success: function(data){
        $('#modal-del-jabatan').modal('hide');    
        getDatajabatan();
      },
      error: function (xhr, ajaxOptions, thrownError) {
          console.error(xhr.responseText);
      }
      }); 
    });
  });
} 

pencarianDataHarian();
function pencarianDataHarian()
{
  $(document).ready(function(){
    $("#cari-date").change(function(){   
      
      $.ajax({
        url: base_url + 'C_report/pencarian_data_harian/'+this.value,
        type: 'POST',
        dataType: "json",
        beforeSend: function(e) {
            
        }, 
        success: function(data){  
          let html = "";
          let no = 1;
          for (let i = 0; i < data.length; i++)
          {  
            html +=  '<tr>'+
                      '<td>'+no+'</td>'+ 
                      '<td>'+data[i].nik+'</td>'+
                      '<td>'+data[i].nama+'</td>'+ 
                      '<td>'+data[i].time_in +'</td>'+ 
                      '<td>'+data[i].keterangan_in+'</td>'+ 
                      '<td>'+data[i].time_out+'</td>'+ 
                      '<td>'+data[i].keterangan_out+'</td>'+ 
                      '<td>'+data[i].status+'</td>'+ 
                  '</tr>'; 
                  no++; 
          }
          $('#data-harian').html(html);
          $('#btn-print').show();
        },
        error: function (xhr, ajaxOptions, thrownError) {
          console.error(xhr.responseText);
        }
      });
    });
  });
}
 